# ==================================================== #
# Helper functions to the parse_calculation_ext function

def eval_string(string):
    string_array = string.replace("(", "").replace(")", "").split()
    string_array[0], string_array[1] = string_array[1], string_array[0]
    return str(eval(' '.join(string_array)))

def is_numeric(string):
    return (True if string.lstrip('-+').replace(".", "").isnumeric() else False)

def build_string(array_string, left_par_index, right_par_index):
    returnString = ""
    for num in range(left_par_index, right_par_index + 1):
        returnString += (array_string[num] if "(" in array_string[num] else (" " + array_string[num]))
    return returnString

def find_para_indexs(array_string):
    for index, chars in enumerate(array_string):
        if "(" in chars: left_par_index = index; continue
        if ")" in chars: right_par_index = index; break
    return left_par_index or 0, right_par_index or 0

# ==================================================== #

# Main string parse function. Calculates nested paranthesis.
def parse_calculation_ext(string):
    # if the passed string is a number, convert it to an int and return
    if is_numeric(string): return int(float(string))

    # Split the string into an array seperated by spaces
    array_string = string.split(" ")

    # Call helping functions to build and calculate values
    built_string = build_string(array_string, *find_para_indexs(array_string))
    updated_string = ' '.join(array_string).replace(built_string, eval_string(built_string))

    # Reccursivly return the altered string
    return parse_calculation_ext(updated_string)

# ==================================================== #
# Test the function and print out the value returned:

# Change boolean to True, if you wish to test the function and print the results
test = False
# Array filled with Strings which will be passed to the function to test each one.
test_array = ["(+ 1 1 )", "3", "(* 4 (- 1 2 ) )", "(/ (- 10 2 ) 4 )", "(- (+ (+ 2 4 ) (* 1 8 ) ) 15 )"]

# If test is true, do function tests:
if test:
    # loop through the test_array and use the string to pass to the parse_calculation_ext function and print result.
    for string in test_array: print(parse_calculation_ext(string))

# ==================================================== #
